<!-- SPACY PROJECT: AUTO-GENERATED DOCS START (do not remove) -->

# 🪐 spaCy Project: Information Extraction from Industry Reports

Prototype for extracting market developements from german industry reports

## 📋 project.yml

The [`project.yml`](project.yml) defines the data assets required by the
project, as well as the available commands and workflows. For details, see the
[spaCy projects documentation](https://spacy.io/usage/projects).

### ⏯ Commands

The following commands are defined by the project. They
can be executed using [`spacy project run [name]`](https://spacy.io/api/cli#project-run).
Commands are only re-run if their inputs have changed.

| Command | Description |
| --- | --- |
| `parse` | Parse XML/PDF documents and convert them to JSON format |
| `annotate` | Annotate documents using rules, generate three datasets for training, validating and testing and store them under ./corpus |
| `train` | Train the neural argument classifier pipeline |
| `evaluate` | Evaluate the classifier on the test data and print the results |
| `predict` | Extract information from industry reports and store them in json-Format |
| `clean` | Remove intermediate files |

### ⏭ Workflows

The following workflows are defined by the project. They
can be executed using [`spacy project run [name]`](https://spacy.io/api/cli#project-run)
and will run the specified commands in order. Commands are only re-run if their
inputs have changed.

| Workflow | Steps |
| --- | --- |
| `all` | `parse` &rarr; `annotate` &rarr; `train` &rarr; `evaluate` &rarr; `predict` |
| `generate_corpus` | `parse` &rarr; `annotate` |

### 🗂 Assets

The following assets are defined by the project. They can
be fetched by running [`spacy project assets`](https://spacy.io/api/cli#project-assets)
in the project directory.

| File | Source | Description |
| --- | --- | --- |
| [`assets/dev`](assets/dev) | Local |  |
| [`assets/eval`](assets/eval) | Local |  |

<!-- SPACY PROJECT: AUTO-GENERATED DOCS END (do not remove) -->
# Running the Prototype
To run this prototype clone the repository using git clone.

## Python Version
In order for the prototype to run Python 3.8 has to be installed.

## Pipenv installation
Packages are managed by Pipenv, which can be installed as follows:

If you\'re using Debian Buster+:

    $ sudo apt install pipenv

Or, if you\'re using Fedora:

    $ sudo dnf install pipenv

Or, if you\'re using FreeBSD:

    # pkg install py36-pipenv

Or, if you\'re using Windows:

    # pip install --user pipenv

When none of the above is an option, it is recommended to use [Pipx](https://pypi.org/p/pipx):

    $ pipx install pipenv

Otherwise, refer to the [documentation](https://pipenv.pypa.io/en/latest/#install-pipenv-today) for instructions.

## Installing required python packages
Once pipenv is installed make sure you are in the root directory of the project and type the following command.

    $ pipenv install

This creates a virtual environment and installs all python packages specified in Pipfile.

## Running the prototype in virtual environment
After installing the required dependencies into a virtual environment, you can spawn a shell as follows:

    $ pipenv shell

Now it is possible to execute the projects commands or workflows from above:

    $ spacy project run [COMMAND|WORKFLOW]
