from typing import List, Tuple, Callable

import spacy
from spacy.tokens import Doc, Span
from thinc.types import Floats2d, Ints1d, Ragged, cast
from thinc.api import Model, Linear, chain, Relu, Softmax, Dropout

Doc.set_extension("trigger_indices", default=[], force=True)

@spacy.registry.architectures.register("arg_model.v1")
def create_arg_model(
    create_instance_tensor: Model[List[Doc], Floats2d],
    classification_layer: Model[Floats2d, Floats2d],
) -> Model[List[Doc], Floats2d]:
    with Model.define_operators({">>": chain}):
        model = create_instance_tensor >> classification_layer
        model.attrs["get_instances"] = create_instance_tensor.attrs["get_instances"]
    return model


@spacy.registry.architectures.register("arg_classification_layer.v1")
def create_classification_layer(
    hidden_width: int = None, input_width: int = None
) -> Model[Floats2d, Floats2d]:
    with Model.define_operators({">>": chain}):
        return Relu(nO = hidden_width, nI = input_width) >> Softmax()


@spacy.registry.misc.register("arg_instance_generator.v1")
def create_instances(max_length: int) -> Callable[[Doc], List[Tuple[Span, Span]]]:
    '''Getting instance to classify from doc ((trigger, argument) pairs)'''
    def get_instances(doc: Doc) -> List[Tuple[Span, Span]]:
        instances = []
        for i in doc._.trigger_indices:
            # using trigger_indices to get the trigger Spans
            trigger = doc[i:i+1]
            for arg in doc:
                # iterating through tokens of the doc and use those as arg candidates 
                # that are in a certain (max_length) distance from the trigger.
                if abs(arg.i-trigger.start) <= max_length:
                    instances.append((trigger, doc[arg.i:arg.i+1]))

        return instances

    return get_instances


@spacy.registry.architectures.register("arg_instance_tensor.v1")
def create_tensors(
    tok2vec: Model[List[Doc], List[Floats2d]],
    pooling: Model[Ragged, Floats2d],
    get_instances: Callable[[Doc], List[Tuple[Span, Span]]],
) -> Model[List[Doc], Floats2d]:

    return Model(
        "instance_tensors",
        instance_forward,
        layers=[tok2vec, pooling],
        refs={"tok2vec": tok2vec, "pooling": pooling},
        attrs={"get_instances": get_instances},
        init=instance_init,
    )


def instance_forward(model: Model[List[Doc], Floats2d], docs: List[Doc], is_train: bool) -> Tuple[Floats2d, Callable]:
    '''Converting a batch of docs to (tigger,arg) instance vectors'''
    #Get layers
    pooling = model.get_ref("pooling")
    tok2vec = model.get_ref("tok2vec")
    get_instances = model.attrs["get_instances"]

    #Get (trigger, arg) pairs
    all_instances = [get_instances(doc) for doc in docs]

    #Convert dos to a token vector matrix
    tokvecs_per_doc, bp_tokvecs = tok2vec(docs, is_train)

    spans = []
    lengths = []

    #iterate through docs and get instances and token vectors for each doc
    for doc_nr, (instances, tokvecs) in enumerate(zip(all_instances, tokvecs_per_doc)):
        token_indices = []
        for instance in instances:
            for span in instance:
                token_indices.extend([i for i in range(span.start, span.end)])
                lengths.append(span.end - span.start)
        spans.append(tokvecs[token_indices])
    lengths = cast(Ints1d, model.ops.asarray(lengths, dtype="int32"))
    entities = Ragged(model.ops.flatten(spans), lengths)
    pooled, bp_pooled = pooling(entities, is_train)

    # Reshape so that pairs of trigger vector and argument vector are concatenated
    pairs = model.ops.reshape2f(pooled, -1, pooled.shape[1] * 2)

    # define a backpropagation function to execute the process backwards
    def backprop(d_arg_predictions: Floats2d) -> List[Doc]:
        d_pooled = model.ops.reshape2f(d_arg_predictions, d_arg_predictions.shape[0] * 2, -1)
        d_ents = bp_pooled(d_pooled).data
        d_tokvecs = []
        ent_index = 0
        for doc_nr, instances in enumerate(all_instances):
            shape = tokvecs_per_doc[doc_nr].shape
            d_tokvec = model.ops.alloc2f(*shape)
            count_occ = model.ops.alloc2f(*shape)
            for instance in instances:
                for ent in instance:
                    d_tokvec[ent.start: ent.end] += d_ents[ent_index]
                    count_occ[ent.start: ent.end] += 1
                    ent_index += ent.end - ent.start
            d_tokvec /= count_occ + 0.00000000001
            d_tokvecs.append(d_tokvec)

        d_docs = bp_tokvecs(d_tokvecs)
        return d_docs

    return pairs, backprop


def instance_init(model: Model, X: List[Doc] = None, Y: Floats2d = None) -> Model:
    tok2vec = model.get_ref("tok2vec")
    if X is not None:
        tok2vec.initialize(X)
    return model
