import spacy
import json
from pathlib import Path
from spacy.tokens import DocBin, Doc, Span, Token
from spacy.matcher import DependencyMatcher
from spacy import Language
from spacy.pipeline import EntityRuler

ARG_LABELS = ["ARG_KNZ", "ARG_FAC", "ARG_AMT", "ARG_TIME", "ARG_REFTIME", "NONE"]
TRI_LABELS = ["TRI_RAISE", "TRI_FALL"]

Doc.set_extension("meta_data", default={})
Doc.set_extension("arg_predictions", default={}, force=True)
Doc.set_extension("trigger_indices", default = [])
Doc.set_extension("events", default=[])
Span.set_extension("is_relevant", default=False)
Token.set_extension("trigger_type", default="")


@Language.factory("arg_ruler", 
requires=["token._.trigger_type", "token.ent_type", "token.dep", "token.pos", "token.lemma"], 
assigns=["doc._.arg_predictions"])
def arg_ruler(nlp: Language, name: str, arg_patterns: Path):
    return ArgRuler(nlp, arg_patterns)

class ArgRuler:
    '''custom pipeline component for rule based event extraction (identifying arguments)'''

    def __init__(self, nlp: Language, arg_patterns: str):
        self.arg_matcher = DependencyMatcher(nlp.vocab, validate=True)
        add_dep_patterns(self.arg_matcher, arg_patterns, self._on_arg_match)


    def _on_arg_match(self, matcher, doc, i, matches):
        match_id, token_ids = matches[i]
        _, patterns = self.arg_matcher.get(match_id)
        trigger = None
        args = []
        for i in range(len(token_ids)):
            token = doc[token_ids[i]]
            label = patterns[0][i]["RIGHT_ID"]
            span = Span(doc, token_ids[i], token_ids[i]+1, label = label)
            if token._.trigger_type.startswith("TRI"):
                trigger = span
            if label.startswith("ARG"):
                args.append(span)

        #Fill arg_predictions for neural network training (gold data)
        for arg in args:
            offset = (trigger.start, arg.start)
            if offset not in doc._.arg_predictions:
                doc._.arg_predictions[offset] = {}
            for label in ARG_LABELS:
                if label == arg.label_:
                    doc._.arg_predictions[offset][label] = 1.0
                else:
                    doc._.arg_predictions[offset][label] = 0.0


    def __call__(self, doc: Doc) -> Doc:
        self.arg_matcher(doc)
        return doc


@Language.factory("trigger_ruler", 
requires=["token.ent_type", "token.dep", "token.pos", "token.lemma"], 
assigns=["token._.trigger_type", "doc._.trigger_indices"] )
def trigger_ruler(nlp: Language, name: str, trigger_patterns: str):
    return TriggerRuler(nlp, trigger_patterns)

class TriggerRuler:
    '''custom pipeline component for rule based event extraction (identifying triggers)'''

    def __init__(self, nlp: Language, trigger_patterns: str):
        self.trigger_matcher = DependencyMatcher(nlp.vocab, validate=True)
        add_dep_patterns(self.trigger_matcher, trigger_patterns, self._on_trigger_match)


    def _on_trigger_match(self, matcher, doc, i, matches):
        match_id, token_ids = matches[i]
        _, patterns = self.trigger_matcher.get(match_id)

        for i in range(len(token_ids)):
            token = doc[token_ids[i]]
            label = patterns[0][i]["RIGHT_ID"]
            if label.startswith("TRI"):
                # Add trigger annotation in order for the rulebased and neural argument classification to work
                token._.trigger_type = label
                doc._.trigger_indices.append(token.i)      


    def __call__(self, doc: Doc) -> Doc:
        self.trigger_matcher(doc)
        return doc


@Language.component("event_annotator", requires=["doc._.arg_predictions"], assigns=["doc._.events"])
def annotate_events(doc: Doc) -> Doc:
    events = []
    for trigger_start in list(set([offset[0] for offset in doc._.arg_predictions])):
        event = {}
        trigger = doc[trigger_start]
        event[trigger._.trigger_type] = {"value": trigger.text, "prediction": "1.0"}

        for offset in [o for o in doc._.arg_predictions if o[0] == trigger_start]:
            arg = doc[offset[1]]
            max_prediction = 0.0
            arg_label = None

            for label in ARG_LABELS:
                label_predict = doc._.arg_predictions[offset][label]
                if  label_predict > max_prediction:
                    max_prediction = label_predict
                    arg_label = label
            if arg_label != "NONE":
                if arg_label not in event or float(event[arg_label]['prediction']) < max_prediction:
                    event[arg_label] = {"value": arg.text, "prediction": str(max_prediction)}
        event["source"] = doc[trigger_start:trigger_start+1].sent.text
        events.append(event)

        #TODO temporal
    doc._.events = []
    doc._.events.extend(events)   
    return doc


def add_dep_patterns(matcher: DependencyMatcher, path: str, on_match):
        path_to_patterns = Path(path)
        pattern_files = [f for f in path_to_patterns.iterdir() if f.is_file() and f.suffix == ".json"]
        for file in pattern_files:
            data = json.loads(file.read_text(encoding="utf8"))
            match_id = data["id"]
            for i, pattern in enumerate(data["patterns"]):
                matcher.add(f"{match_id}_{i}", [pattern], on_match=on_match)