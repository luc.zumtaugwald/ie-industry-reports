from __future__ import annotations

import typer
from wasabi import msg
from pathlib import Path
from typing import Any
import re
import xml.etree.ElementTree as ET
import json
from progress.bar import IncrementalBar

from io import StringIO

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser


def clean_text(text: str) -> str:
    '''cleans the text to prepare it for NLP'''
    # remerge seperated words
    text = re.sub(r"-\s*\n(?=[a-zäöüß])", "", text)
    # normalize spaces
    text = re.sub(r"\s\s+", " ", text)
    # remove header and footer on each page (Sparkassen)
    text = re.sub(r"[0-9]*\s*\n\s*\u00A9.*\nBranchenreport.*\n", " ", text)
    # replace listing symbol \u25ba with dot
    text = re.sub(r"\s*\u25ba\s*", ". ", text)
    # remove line-breaks => spacy is capable of detecting sentence boundaries (for headlines without dot in the end)
    text = re.sub(r"\s*\n\s*", " ", text)
    return text


def read_pdf(file_path: Path) -> str:
    '''Extracts text from a PDF document using Pdfminer.six'''
    text = StringIO()
    with file_path.open('rb') as in_file:
        parser = PDFParser(in_file)
        doc = PDFDocument(parser)
        rsrcmgr = PDFResourceManager()
        device = TextConverter(rsrcmgr, text, laparams=LAParams(char_margin=25))
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.create_pages(doc):
            interpreter.process_page(page)
    
    return text.getvalue()


def process_pdf(path: Path) -> str:
    try:
        text = read_pdf(path)
    except Exception as ex:
        msg.fail(f"Error occured while processing {path}: {ex}")
        return None
    if not text:
        msg.fail(f"Extraction from {path} failed")
        return None
    else:
        return clean_text(text)


def get_meta_data(xml_path: Path) -> dict[str, Any]:
    '''retrieves metadata for an industry report'''
    meta_data = {}

    tree = ET.parse(xml_path)
    root = tree.getroot()

    # Branchen
    wzcodes = root.findall("{*}allgemeine-angaben/{*}branchen/{*}branchencode")
    industry_names = root.findall("{*}allgemeine-angaben/{*}branchen/{*}branchenname")
    meta_data["industries"] = []
    for wzcode, industry_name in zip(wzcodes, industry_names):
        meta_data["industries"].append({
            "wz_code": wzcode.text.strip(),
            "industry_name": industry_name.text.replace("\n", " ").strip()})

    # Herausgeber
    institute = root.find("{*}bibliographische-angaben/{*}autor/{*}institution")
    if institute is not None and institute.text:
        meta_data['institute'] = institute.text.replace("\n", " ").strip()

    # Erscheinungsdatum
    date = root.find("{*}bibliographische-angaben/{*}daten/{*}datum")
    if date is not None and 'normdat' in date.attrib:
        meta_data['date'] = date.attrib['normdat'].strip()

    # Quelle (PDF)
    extern_files = root.findall("{*}externe-dateien/{*}datei")
    pdf_name = next((f.attrib['name'] for f in extern_files if f.attrib.get('name') and f.attrib.get('name').endswith('.pdf')), None)
    if pdf_name is not None:
        meta_data['pdf_name'] = pdf_name.strip()

    return meta_data


def get_data(xml_path: Path, path_to_pdfs: Path) -> tuple[dict[str, Any], str]:
    '''Generates report data using the metadata in xml-format'''
    meta_data = get_meta_data(xml_path)
    text = None

    if 'pdf_name' in meta_data:
        pdf_path = path_to_pdfs.joinpath(meta_data['pdf_name'])
        text = process_pdf(pdf_path)

    return meta_data, text


def main(path_to_files: Path, output_path: Path) -> None:
    '''Parsing xml and pdf files and converting them to JSON'''
    count = 0
    if not path_to_files.is_dir():
        msg.fail(f"{path_to_files} is no directory")
        return
    elif not path_to_files.is_dir():
        msg.fail(f"{path_to_files} is no directory")
        return
    else:
        xmls = [x for x in path_to_files.iterdir() if x.is_file() and x.suffix == ".xml"]
        if xmls:
            print()
            msg.info(f"Parsing XML/PDF documents from {path_to_files}")
            with IncrementalBar('Processing', max=len(xmls)) as bar:
                for xml in xmls:
                    meta_data, text = get_data(xml, path_to_files)
                    if not text:
                        continue
                    json_str = json.dumps({"meta_data": meta_data, "text": text}, ensure_ascii=False, indent=4)
                    save_path = output_path.joinpath(xml.name.replace(".xml", ".json"))
                    save_path.write_text(json_str, encoding="utf8")
                    count += 1
                    bar.next()

    if count > 0 and xmls:
        print()
        msg.good(f"Extracted text data from {count}/{len(xmls)} PDF documents.")
        msg.good(f"Extracted meta data from {count}/{len(xmls)} XML documents.")
        msg.info(f"JSON files were stored to '{output_path}'")

    else:
        msg.fail(f"No data was extracted. Make sure '{path_to_files}' contains documents (.pdf) and their corresponding meta data (.xml).")
    print()


if __name__ == '__main__':
    typer.run(main)
