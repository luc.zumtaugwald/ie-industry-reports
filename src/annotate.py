import typer
import spacy
import json
import random
from utils import read_ent_patterns
from pathlib import Path
from wasabi import msg
from spacy.tokens import DocBin, Doc, Span, Token
from spacy.matcher import DependencyMatcher
from spacy import Language
from progress.bar import IncrementalBar
from spacy.pipeline import EntityRuler
import pipeline.rulers


def _is_sent_relevant(sent: Span) -> bool:
    '''Determines if sentence is relevant for further processing'''
    has_triggers = any([t._.trigger_type.startswith("TRI") for t in sent])
    has_entities = any([t.ent_type_ != "" for t in sent])
    has_verb = any([t.pos_ == "VERB" for t in sent])
    return has_triggers and has_entities and has_verb

def run_pipe(nlp: Language, text: str, sents_per_doc: int) -> Doc:
    '''Takes a text, generates a list of docs containing sents_per_doc sentences each and passes them through a pipeline'''
    doc = nlp(text)  
    sents = [s.text for s in doc.sents if _is_sent_relevant(s)]
    n = sents_per_doc
    sent_bags = [" ".join(sents[i:i+n]) for i in range(0, len(sents), n)]

    return [d for d in nlp.pipe(sent_bags)]


def build_annotation_pipeline(ent_patterns: Path, trigger_patterns: Path, arg_patterns) -> Language:
    '''builds a rulebased pipeline using linguistic annotations from pretrained spacy model'''
    # load base model for tokenzing, pos-tagging and lemmatizing
    nlp = spacy.load('de_core_news_md', exclude=["ner"])
    patterns = read_ent_patterns(ent_patterns)
    nlp.add_pipe('entity_ruler').add_patterns(patterns)
    nlp.add_pipe('merge_entities')
    nlp.add_pipe('trigger_ruler', config={"trigger_patterns": str(trigger_patterns)})
    nlp.add_pipe('arg_ruler', config={"arg_patterns": str(arg_patterns)})
    return nlp


def main(input_path: Path, ent_patterns: Path, trigger_patterns: Path, arg_patterns: Path, output_path: Path, sents_per_doc: int = 1):
    if not input_path.is_dir():
        msg.fail(f"'{input_path}' is not a directory")
        return
    files = [f for f in input_path.iterdir() if f.name.endswith('.json')]
    if not files:
        msg.fail(f"'{input_path}' is empty")
        return

    nlp = build_annotation_pipeline(ent_patterns, trigger_patterns, arg_patterns)

    train_path = output_path.joinpath("train.spacy")
    dev_path = output_path.joinpath("dev.spacy")
    test_path = output_path.joinpath("test.spacy")

    # Creating three datasates for training, validation and testing
    train_docs = []
    dev_docs = []
    test_docs = []

    print()
    msg.info("Annotating documents using rules")
    with IncrementalBar('Processing', max=len(files)) as bar:
        for file in files:
            data = json.loads(file.read_text(encoding="utf8"))
            docs = run_pipe(nlp, data["text"], sents_per_doc)
            for doc in docs:
                rand = random.random()
                if  rand <= 0.7:
                    # 70% of docs for training 
                    train_docs.append(doc)
                elif rand <=0.9:
                    # 20% of docs for validation
                    dev_docs.append(doc)
                else:
                    # 10% of docs for testing
                    test_docs.append(doc)
            bar.next()

    # creating datasets in spacys format storing custom annotations
    train_bin = DocBin(docs=train_docs, store_user_data=True)
    train_bin.to_disk(train_path)

    dev_bin = DocBin(docs=dev_docs, store_user_data=True)
    dev_bin.to_disk(dev_path)

    test_bin = DocBin(docs=test_docs, store_user_data=True)
    test_bin.to_disk(test_path)
    print()
    msg.good(f"Annotations saved to '{output_path}'")
    msg.good(f"Stored {len(train_docs)} Doc-Objects for training to {train_path}")
    msg.good(f"Stored {len(dev_docs)} Doc-Objects for validation to {dev_path}")
    msg.good(f"Stored {len(test_docs)} Doc-Objects for evaluation to {test_path}")


if __name__ == "__main__":
    typer.run(main)
