import json
from pathlib import Path


def read_ent_patterns(ent_patterns: Path):
    '''Reads entity patterns from a directory containing json-formatted pattern files for entities'''
    patterns = []
    files = [f for f in ent_patterns.iterdir() if f.name.endswith('.json')]
    for file in files:
            data = json.loads(file.read_text(encoding="utf8"))
            for pattern in data['patterns']:
                pattern_line = {"label": data["label"], "pattern": pattern}
                patterns.append(pattern_line)
    return patterns