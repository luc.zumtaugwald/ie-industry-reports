import typer
import spacy
from spacy import Language
from pathlib import Path
from progress.bar import IncrementalBar
from utils import read_ent_patterns

import pipeline.rulers
import pipeline.arg_classifier
import pipeline.arg_model

from annotate import run_pipe
import json
from wasabi import msg


def build_prediction_pipeline(ent_patterns: Path, trigger_patterns: Path, arg_patterns: Path, model_path: Path, rules: bool) -> Language:
    '''builds a prediction pipeline combining rules for entities and triggers with a neural classifier for event arguments'''
    #load base model in order for the linguistic annotations to work
    nlp = spacy.load('de_core_news_md', exclude=["ner"])
    patterns = read_ent_patterns(ent_patterns)
    nlp.add_pipe('entity_ruler').add_patterns(patterns)
    nlp.add_pipe('merge_entities')
    nlp.add_pipe('trigger_ruler', config={"trigger_patterns": str(trigger_patterns)})
    if rules:
        nlp.add_pipe('arg_ruler', config={"arg_patterns": str(arg_patterns)})
    else:
        #load trained classifier
        trained_model = spacy.load(str(model_path))
        nlp.add_pipe('arg_classifier', source=trained_model, last=True)

    nlp.add_pipe('event_annotator')
    return nlp


def process_data(file: Path, nlp: Language, output_path: Path):
    '''run prediction pipeline on a file'''
    data = json.loads(file.read_text(encoding="utf8"))
    docs = run_pipe(nlp, data["text"], 1)
    events = []
    for doc in docs:
        events.extend(doc._.events)

    #only add events with useful information
    data["events"] = [e for e in events if "ARG_KNZ" in e and ("ARG_FAC" in e or "ARG_AMT" in e) and "ARG_TIME" in e]
    del data["text"]
    json_str = json.dumps(data, ensure_ascii=False, indent=4)
    #save predictions in json-format
    save_path = output_path.joinpath(f"pred_{file.name}")
    save_path.write_text(json_str, encoding="utf8")


def main(path_to_data: Path, output_path: Path, path_to_ents: Path, path_to_triggers: Path, path_to_args: Path, path_to_model: Path, use_rules: bool = False) -> None:
    '''Predicting events from xml and pdf files and converting them to JSON'''
    nlp = build_prediction_pipeline(path_to_ents, path_to_triggers, path_to_args, path_to_model, use_rules)
    
    files = [f for f in path_to_data.iterdir() if f.name.endswith('.json')]
    msg.info("Extracting information from documents")
    with IncrementalBar('Processing', max=len(files)) as bar:
        for file in files:
            process_data(file, nlp, output_path)
            bar.next()

if __name__ == '__main__':
    typer.run(main)