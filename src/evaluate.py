import random
import typer
from pathlib import Path
import spacy
from spacy.tokens import DocBin, Doc
from spacy.training.example import Example

# make the factory work
from pipeline.arg_classifier import score_args
import pipeline.arg_model


def main(trained_pipeline: Path, test_data: Path):
    nlp = spacy.load(trained_pipeline)

    # Read test data and generate examples
    doc_bin = DocBin(store_user_data=True).from_disk(test_data)
    docs = doc_bin.get_docs(nlp.vocab)
    examples = []
    for gold in docs:
        pred = Doc(
            nlp.vocab,
            words=[t.text for t in gold],
            spaces=[t.whitespace_ for t in gold],
        )
        pred.ents = gold.ents
        pred._.trigger_indices.extend(gold._.trigger_indices)
        for name, proc in nlp.pipeline:
            pred = proc(pred)
        examples.append(Example(pred, gold))

    # create random examples for comparison
    random_examples = []
    docs = doc_bin.get_docs(nlp.vocab)
    for gold in docs:
        pred = Doc(
            nlp.vocab,
            words=[t.text for t in gold],
            spaces=[t.whitespace_ for t in gold],
        )
        pred.ents = gold.ents
        pred._.trigger_indices.extend(gold._.trigger_indices)
        classifier = nlp.get_pipe("arg_classifier")
        get_instances = classifier.model.attrs["get_instances"]
        for (trigger, arg) in get_instances(pred):
            offset = (trigger.start, arg.start)
            if offset not in pred._.arg_predictions:
                pred._.arg_predictions[offset] = {}
            for label in classifier.labels:
                pred._.arg_predictions[offset][label] = random.uniform(0, 1)
        random_examples.append(Example(pred, gold))

    # define thresholds to be evaluated.
    thresholds = [0.000, 0.050, 0.100, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 0.999]
    print()
    print("Random baseline:")
    _score_and_format(random_examples, thresholds)

    print()
    print("Results of the trained model:")
    _score_and_format(examples, thresholds)


def _score_and_format(examples, thresholds):
    for threshold in thresholds:
        r = score_args(examples, threshold)
        results = {k: "{:.2f}".format(v * 100) for k, v in r.items()}
        print(f"threshold {'{:.2f}'.format(threshold)} \t {results}")


if __name__ == "__main__":
    typer.run(main)
